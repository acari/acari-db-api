# ACARI db api

# Building

`yarn && yarn build`

## Running with docker compose (Recommended)

`docker-compose up`

## Running with docker

Running with docker requires mongo that is running on the host machine.

`docker build -t acari-db-api .`

`docker run -t -p 3001:3001 acari-db-api`

# Specs

This project uses `jest` as test framework.
To run specs `npm test`

## Routes

### /gameserver/:ip

Send an ip this way to let the server know this IP is alive. For example: `db.api.url/gameserver/127.0.0.1`

### /listservers

This will produce a list of all servers currently alive.

### /users/add/nick

Add new user to active users list

curl -H "Content-Type: application/json" -X POST -d '{"nickName":"nick"}' http://localhost:3001/users/add
