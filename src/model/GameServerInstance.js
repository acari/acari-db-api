// @flow

import mongoose from 'mongoose'

const GameServerInstance = mongoose.model('GameServerInstance', {
  location: String,
  timeStamp: Date
})

export default GameServerInstance
