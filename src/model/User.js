// @flow

import mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
  userId: String,
  nickName: String,
  online: Boolean,
  socketId: String,
  lastSeen: Date
})

export type UserType = {
  userId: string,
  nickName: string,
  online: boolean,
  socketId: string,
  lastSeen: Date
}

export default mongoose.model('User', userSchema)
