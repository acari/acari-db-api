// @flow

import mongoose from 'mongoose'

mongoose.Promise = require('bluebird')

const ChatLog = mongoose.model('ChatLog', {
  timestampStart: Date,
  timestampEnd: Date,
  messages: [{
    timestamp: Date,
    socketId: String,
    nickName: String,
    userId: String,
    message: String
  }]
})

export default ChatLog
