// @flow

import bluebird from 'bluebird'
import express from 'express'
import bodyParser from 'body-parser'
import winston from 'winston'
import expressWinston from 'express-winston'
import http from 'http'
import mongoose from 'mongoose'
import path from 'path'
import SocketIO    from 'socket.io'

import lobbyServer from './lobby-server'
import gameServer  from './game-server'
import ChatService from './service/ChatService.js'
import UserService from './service/UserService.js'

import type { $Application, $Request, $Response } from 'express'
import type { ServerConfig } from './config'
import type { ChatMessage } from './service/ChatService'

mongoose.Promise = bluebird

let server: any = null
let refreshUserStatusInterval: any = null

export function startServer(config: ServerConfig): void {
  if (server) {
    winston.error('Server is already running')
    return
  }

  const app: $Application = express()
  app.use(bodyParser.json())

  configureWinston(app, config)

  server = http.createServer(app)

  mongoose.connect(`mongodb://${config.mongoPath}/acari`)

  const userService = new UserService()
  const chatService = new ChatService()

  configureRoutes(app)
  configureSocketServer(userService, chatService, config)

  server.listen(config.port)

  winston.info(`Server listening in port ${config.port}`)
  winston.debug('Using configuration', config)
}

function configureWinston(app: $Application, config: ServerConfig): void {
  const consoleLogConfig = new winston.transports.Console({
    level: config.logLevel,
    json: false,
    colorize: true
  })

  const fileLogConfig = new winston.transports.File({
    filename: config.logFilePath,
    level: config.logLevel
  })

  winston.configure({
    transports: [
      consoleLogConfig,
      fileLogConfig
    ]
  })

  app.use(expressWinston.logger({
    transports: [
      consoleLogConfig,
      fileLogConfig
    ],
    expressFormat: true,
    colorize: true,
    meta: false,
    msg: 'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}} {{req.body}} {{res.body}}'
  }))
}

function configureRoutes(app: $Application): void {
  app.use('/servers/add/:ip', gameServer.addServer)
  app.use('/servers/list', gameServer.getServerList)

  app.post('/users/add/:nickName', lobbyServer.addUser)
  app.get('/users/list', lobbyServer.getUsers)
  app.get('/chat/message', lobbyServer.addMessage)

  app.get('/', (req: $Request, res: $Response) => {
    res.sendFile(path.join(__dirname, '/index.html'))
  })
}

function configureSocketServer(
  userService: any,
  chatService: any,
  config: ServerConfig
): void {
  const socketServer = SocketIO(server)

  socketServer.on('connection', (socket) => {
    winston.info('awesome, a lobby server connected: ', socket.id)

    socket.on('message', (message: ChatMessage) => {
      const patchedMessage: ChatMessage = Object.assign({}, message, {userId: socket.id})

      chatService.insertMessage(patchedMessage).then(() => {
        emitMessageList()
      })
    })

    socket.on('userJoined', (data) => {
      winston.info('User joined', data)
      userService.addUser(data).then(() => {
        emitUserList()
        emitMessageList()
      })
    })

    socket.on('userLeft', (data) => {
      winston.info('User Left', data)
      userService.removeUser(data).then(() => {
        emitUserList()
      })
    })

    socket.on('users', (data) => {
      winston.info('Users requested', data)
      emitUserList()
    })

    socket.on('reportLocalUsers', (data) => {
      winston.info('Lobby', socket.id, ' is reporting users: ', data)
      userService.updateUsers(data)
    })

    socket.on('messages', (data) => {
      winston.info('Messages requested', data)
      emitMessageList()
    })

    function emitUserList() {
      winston.info('Sending userlist to sockets')
      userService.retrieveUserList().then(userList => {
        winston.debug('userList', userList)
        socket.emit('userList', userList)
      })
    }

    function emitMessageList() {
      chatService.getNewestMessageChunk().then(chatLog => {
        winston.info('Emitting', chatLog[0].messages.length, 'messages')
        socket.emit('messageList', chatLog[0].messages)
      })
    }
  })


  refreshUserStatusInterval = setInterval(() => {
    winston.info('Refreshing user status')
    socketServer.sockets.emit('refreshUsers')
  }, config.refreshUserStatusIntervalMs)
}

export function stopServer(callback: Function): void {
  if (server) {
    winston.info('Shutting server down...')
    clearInterval(refreshUserStatusInterval)
    server.close(callback)
    server = null
  }
}
