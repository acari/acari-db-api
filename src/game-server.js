// @ flow

import winston from 'winston'
import GameServerInstance from './model/GameServerInstance'

import type { $Request, $Response } from 'express'

function addServer(req: $Request, res: $$Response): void {
  const ip:string = req.params.ip

  GameServerInstance.findOne({location: ip}, (err, instance) => {
    if (err) {
      winston.error('Error when trying to find object:', err)
      return res.status(500).send('Internal server error')
    } else {
      if (instance) {
        winston.info('Updating ', instance.location)
        instance.timeStamp = new Date()
      } else {
        winston.info('Creating new instance ' + ip + ' to DB')
        instance = new GameServerInstance({location: ip, timeStamp: new Date()})
      }

      instance.save((err, instance) => {
        if (err) {
          winston.error('Error saving instance..')
          return res.status(500).send('Internal server error')
        } else {
          winston.info('Saved successfully!')
          res.send({ message: 'ok' })
        }
      })
    }
  })
}

function getServerList(req: $Request, res: $Response): void {
  GameServerInstance.find({}, (err, instances) => {
    if (err) {
      winston.error('error while getting server list', err)
      return res.status(500).send('Internal server error')
    }

    winston.info('Returning serverlist..')
    res.send({ serverlist: instances })
  })
}

export default {
  addServer,
  getServerList
}
