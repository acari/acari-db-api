// @flow

import winston from 'winston'
import ChatLog from '../model/ChatLog'

export type ChatMessage = {
  timestamp: string,
  socketId: string,
  nickName: string,
  userId: string,
  message: string
}

class ChatService {
  async insertMessage(message: ChatMessage): Promise<void> {
    // // Find latest message chunk from db
    // // if empty db
    // //   create new chunk
    // // Check fetched chunk size
    // // if chunkSize < maxChunkSize
    // //   add message to chunk
    // // else
    // //   create new chunk

    winston.info('Insert message', message)

    await ChatLog
      .findOne()
      .sort({created_at: -1})
      .limit(1)
      .exec()
      .then(async (instance) => {
        if (!instance) {
          winston.info('message chunk not found, creating new one')
          instance = new ChatLog({
            timestampStart: message.timestamp,
            timestampEnd: message.timestamp,
            messages: [message]
          })
        } else {
          winston.info('message chunk found')
          instance.messages.push(message)
        }

        await instance.save().then((instance) => {
          winston.info('User saved successfully!')
        })
      })
  }

  getNewestMessageChunk() {
    return ChatLog.find().sort({'timestampEnd': -1}).limit(1)
  }

}

export default ChatService
