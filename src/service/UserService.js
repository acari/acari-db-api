// @flow

import User from '../model/User'
import moment from 'moment'
import winston from 'winston'

import type { UserType } from '../model/User'

class UserService {

  updateUsers(localUsers: Array<UserType>): void {
    localUsers.forEach(user => {
      User.findOneAndUpdate({nickName: user.nickName}, {$set: {lastSeen: user.lastSeen}}, (err, instance) => {
        if (err) {
          winston.error('Something went wrong with update', err)
        } else {
          winston.info('Lastseen updated for', user.nickName)
        }
      })
    })

    const twentySecondsAgo = moment().subtract(20, 'seconds')

    User.find({online: true, lastSeen: {$lt: twentySecondsAgo.toDate()}}, function (err, instances) {
      if (err) {
        winston.error('Something went wrong with search for expired users', err)
      } else if (instances.length === 0) {
        // No expired users
      } else {
        winston.info('Setting', instances.length, 'expired users offline')
        instances.forEach(user => {
          user.online = false
          user.save((err, instance) => {
            if (err) {
              winston.error('User offline set went wrong :(', err)
            } else {
              winston.info(user.nickName, 'is set offline')
            }
          })
        })
      }
    })
  }

  retrieveUserList() {
    return User.find({online: true}, (err, instances) => {
      if (err) {
        winston.error('error while retrieving user list', err)
        return 'Error'
      }
      winston.info('Found users', instances.length)
    })
  }

  addUser(userData: UserType): void {
    const nickName: string = userData.nickName
    const socketId: string = userData.socketId
    winston.info('userData', userData)

    return User.findOne({nickName: nickName}, (err, instance) => {
      if (err) {
        winston.error('Error when trying to find user:', nickName, err)
      } else {
        if (instance) {
          winston.info('User already exists, setting online')
          instance.online = true
        } else {
          winston.info('New user! Saving to db')
          instance = new User({
            nickName: nickName,
            socketId: socketId,
            online: true,
            lastSeen: Date.now()
          })
        }
        instance.save((err, instance) => {
          if (err) {
            winston.error('Error saving user..')
          } else {
            winston.info('User saved successfully!')
          }
        })
      }
    })
  }

  removeUser(nickName: string): void {
    winston.info('Removing user', nickName)

    return User.findOne({nickName: nickName}, (err, instance) => {
      if (err) {
        winston.error('Error when trying to find user that\'s leaving:', nickName, err)
      } else {
        if (instance) {
          winston.info('User', instance.nickName, 'left')
          instance.online = false
          instance.lastSeen = Date.now()
          instance.save((err, instance) => {
            if (err) {
              winston.error('Error saving leaving user..')
            } else {
              winston.info('User set to offline successfully!')
            }
          })
        } else {
          winston.info('Trying to log off a user that doesn\'t exist?')
        }
      }
    })
  }
}

export default UserService
