// @flow

import config from './config'
import { startServer, stopServer } from './server'

startServer(config)

process.on('SIGINT', () => {
  stopServer(() => {
    process.exit(0)
  })
})
