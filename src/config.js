// @flow

export type ServerConfig = {
  port: number,
  logFilePath: string,
  logLevel: string,
  mongoPath: string,
  refreshUserStatusIntervalMs: number
}

const serverConfig: ServerConfig = {
  port: 3001,
  logFilePath: 'dev-log.log',
  logLevel: 'debug',
  mongoPath: process.env.MONGODB_URL || 'localhost',
  refreshUserStatusIntervalMs: 5000
}

export default serverConfig
