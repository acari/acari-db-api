// @flow

import winston from 'winston'
import User from './model/User'
import ChatLog from './model/ChatLog'

import type { $Request, $Response } from 'express'

function addUser(req: $Request, res: $Response): void {
  const nickName:string = req.params.nickName
  winston.info('nickName', nickName)

  User.findOne({nick: nickName}, (err, instance) => {
    if (err) {
      winston.error('Error when trying to find user:', nickName, err)
      return res.status(500).send('Internal server error')
    } else {
      if (instance) {
        winston.info('User already exists, setting online')
        instance.online = true
      } else {
        winston.info('New user! Saving to db')
        instance = new User({
          nick: nickName,
          online: true
        })
      }
      instance.save((err, instance) => {
        if (err) {
          winston.error('Error saving user..')
          return res.status(500).send('Internal server error')
        } else {
          winston.info('User saved successfully!')
          res.send({ message: 'ok' })
        }
      })
    }
  })
}

function getUsers(req: $Request, res: $Response): void {
  User.find({}, (err, instances) => {
    if (err) {
      winston.error('error while retrieving user list', err)
      return res.status(500).send('Internal server error')
    }

    res.send({ userlist: instances })
  })
}

function getChatLog(req: $Request, res: $Response): void {
  ChatLog.find({}, (err, instances) => {
    if (err) {
      winston.error('error while retrieving chat log', err)
      return res.status(500).send('Internal server error')
    }
    res.send(instances)
  })
}

function addMessage(req: $Request, res: $Response): void {
  winston.info('Would love to add a message here')
}

export default {
  addUser,
  getUsers,
  getChatLog,
  addMessage
}
