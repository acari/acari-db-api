FROM node:6.7.0

EXPOSE 3001

ADD package.json /acari-db-api/package.json

# Needed to run babel inside docker container.
RUN cd $(npm root -g)/npm && npm install fs-extra && sed -i -e s/graceful-fs/fs-extra/ -e s/fs.rename/fs.move/ ./lib/utils/rename.js

WORKDIR /acari-db-api
RUN npm install --silent
RUN npm install yarn babel-cli

ADD . /acari-db-api
CMD npm start
