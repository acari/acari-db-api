import mongoose    from 'mongoose'
import specSupport from './spec-support'
import ChatService from '../src/service/ChatService'

describe('chat log tests', () => {
  let connection = null
  const chatService = new ChatService()

  beforeAll((done) => {
    mongoose.connect(`mongodb://localhost/acari-db-test`)
    connection = mongoose.connection

    connection.on('error', (err) => {
      done.fail(err)
    })

    connection.once('open', () => {
      done()
    })
  })

  afterAll(() => {
    connection.db.dropDatabase()
    connection.close()
  })

  describe('adding new messages', () => {
    it.skip('should create new chunk when adding message into empty db', async (done) => {
      const req = specSupport.mockRequest(null, {
        timestamp: new Date().toISOString(),
        userId: 'fjaofad3rerw', // TODO: maybe we should create specSupport.createTestUser() instead of blaa blaa data
        message: 'jei'
      })

      const res = specSupport.mockResponse()

      // await chatLog.specs.insertMessage(new Date(), '123456789', 'testUser', 'fjaofad3rerw', 'jei')

      console.log('done')

      expect(res.resData.statusCode).toEqual(200)
      expect(res.resData.data).toEqual('ok')

      done()
    })

    // it('should add new message into chunk', () => {
    // })
    //
    // it('should create new chunk if previous chunk is not found', () => {
    // })
  })
})
