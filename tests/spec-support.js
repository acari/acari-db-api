// Support function to add spec support for modules
// - output: object of exported functions
// - specs: exported specs functions, only visible when testing
function exportFn(output, specs) {
  if (process.env.EXPORT_SPECS) {
    return {output, specs: specs}
  }

  return output
}

function mockRequest(params, body) {
  return {
    params: params,
    body: body
  }
}

function mockResponse() {
  const resData = {
    statusCode: 200
  }

  return {
    status: (statusCode) => {
      resData.statusCode = statusCode
    },
    send: (data) => {
      resData.data = data
    },
    resData
  }
}

export default {
  export: exportFn,
  mockRequest,
  mockResponse
}
